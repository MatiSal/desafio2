import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth'
import firebase from 'firebase/compat/app';
import { first } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afauth : AngularFireAuth) { }

  async login(email: string, password: string ){
    try{
      return await this.afauth.signInWithEmailAndPassword(email,password);
    } catch (err){
      console.log("error: ", err);
      return null
    }
  }

  async loginWithGoogle(email: string, password: string){
    try{
      return await this.afauth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    } catch (err){
      console.log("error con google: ", err);
      return null
    }
  }

  async register(email: string, password: string ){
    try{
      return await this.afauth.createUserWithEmailAndPassword(email,password);
    } catch (err){
      console.log("error: ", err);
      return null
    }
  }

  getUser(){
    return this.afauth.authState.pipe(first()).toPromise();
  }

  async getUid(){
    const user =  await this.afauth.currentUser
    if (user === null){
      return null
    }
    else 
    return user?.uid
  }

  logout(){
    this.afauth.signOut()
  }
}
