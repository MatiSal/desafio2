import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Movie } from '../interfaces/movie';
import { MovieSeries, MoviSerieUser } from '../interfaces/movie-series';
import { Tv } from '../interfaces/tv';
import { AngularFirestore, CollectionReference } from '@angular/fire/compat/firestore';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private api_Key: string = "6038301c6c6c5835a6766a2c94f2deea"
  private baseUrl: string = 'https://api.themoviedb.org/3'
  
  constructor(
    private _http: HttpClient,
    private firestore: AngularFirestore
  ) { }

  getTrending(): Observable<MovieSeries[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<MovieSeries[]>(this.baseUrl + '/trending/all/week', {
      params: params
    })
  }

  getMovies(): Observable<Movie[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<Movie[]>(this.baseUrl + '/movie/popular', {
      params: params
    })
  }

  getSeries(): Observable<Tv[]> {
    let params = new HttpParams().set('api_key', this.api_Key)
    return this._http.get<Tv[]>(this.baseUrl + '/tv/popular', {
      params: params
    })
  }

  getSearch(item): Observable<MovieSeries[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<MovieSeries[]>(this.baseUrl + '/search/multi?' + '&query=' + item, {
      params: params
    })
  }
  getSearchIdMovie(item): Observable<MovieSeries[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<MovieSeries[]>(this.baseUrl + '/movie/' + item + '?', {
      params: params
    })
  }

  getSearchIdSerie(item): Observable<MovieSeries[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<MovieSeries[]>(this.baseUrl + '/tv/' + item + '?', {
      params: params
    })
  }
  getSearchSerie(item): Observable<Tv[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<Tv[]>(this.baseUrl + '/search/tv?' + '&query=' + item, {
      params: params
    })
  }

  getSearchMovie(item): Observable<Movie[]> {
    let params = new HttpParams().set('api_key', this.api_Key);
    return this._http.get<Movie[]>(this.baseUrl + '/search/movie?' + '&query=' + item, {
      params: params
    })
  }

  public adUser(uid:string) : Promise<any>{
    return this.firestore.collection('Usuarios').doc(uid).set({
    })
    .then (()=>{
      console.log(uid)
      console.log("se guardo")
    })
    .catch((error)=>{
      console.error("error: ",error)
    })
  }

  public addMovieSerie(value: MovieSeries,id:string){
    if (value.media_type == 'tv')
      return this.firestore.collection('Usuarios').doc(id).collection('tv').add(value);
    else
    return this.firestore.collection('Usuarios').doc(id).collection('movie').add(value);
  }

  public getLMovieSeries(id:string,type:string):Observable<any>{

    return this.firestore.collection('Usuarios').doc(id).collection(type).snapshotChanges()
    
  }

 col(id:string,type:string , queryFn?): Observable<any[]>{
    return this.firestore.collection('Usuarios').doc(id).collection(type,queryFn).snapshotChanges().pipe(
      map(docs=>{
        return docs.map(d =>{
          const data = d.payload.doc.data();
          const id = d.payload.doc.id;
          return{id, ...data}
        })
      })
    )
 }


  public deleteMovieSerie(uid: string, idg: string,type :string){
    if(type == 'tv')
      return this.firestore.collection(`Usuarios/${uid}/tv`).doc(idg).delete()
      
    else
      return this.firestore.collection(`Usuarios/${uid}/movie`).doc(idg).delete()
      .then(()=>
      console.log("documento borrado"))
      .catch((error) => console.error("elemento no borrado",error))
  }

}
