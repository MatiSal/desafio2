import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user$: Observable<any> = this.authService.afauth.user
  isLogged: boolean = false
  constructor(private authService: AuthService, private router:Router) { }

  async ngOnInit() {
  }

  logout() {
    this.authService.logout()
    this.isLogged = false
    localStorage.removeItem('Usuario');
    this.router.navigate(['/Inicio']);
  }
}
