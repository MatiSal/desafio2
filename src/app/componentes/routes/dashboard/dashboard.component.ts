import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { MovieSeries } from 'src/app/interfaces/movie-series';
import { MoviesService } from 'src/app/services/movies.service';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  movieseries: Array<MovieSeries> = new Array<MovieSeries>();
  movieseries2: Array<MovieSeries> = new Array<MovieSeries>();
  user: any
  movieC: number = 0
  tvC: number = 0
  constructor(private movieSerieService: MoviesService) { }

  ngOnInit(): void {
    this.getCantidadM()
    this.getCantidadS()
  }
  
  getLocalStorage(){ 
    this.user = JSON.parse(localStorage.getItem('Usuario')||'{}')
    return this.user.uid
  }

 getCantidadM() {
  this.movieSerieService.getLMovieSeries(this.getLocalStorage(),'movie').subscribe((values )=>{
    this.movieC = values.length});
  }
  getCantidadS() {
    this.movieSerieService.getLMovieSeries(this.getLocalStorage(),'tv').subscribe((values )=>{
      this.tvC = values.length});
    }
  
}
