import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faTv } from '@fortawesome/free-solid-svg-icons';
import { MovieSeries } from 'src/app/interfaces/movie-series';
import { MoviesService } from 'src/app/services/movies.service';
import { Tv } from 'src/app/interfaces/tv';
import { Movie } from 'src/app/interfaces/movie';
import { Genero } from 'src/app/interfaces/genero';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {
  series: Tv;
  movie: Movie
  media: any
  flag: boolean
  constructor(private _Search: MoviesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getId()
  }

  getId() {
    let id = this.route.snapshot.paramMap.get('id');
    this.media = this.route.snapshot.paramMap.get('media');
    console.log(this.media)
    if (this.media == 'tv') {
      this._Search.getSearchIdSerie(id).subscribe((res: any) => {
        this.flag = true;
        console.log(this.series = res)
      })

    }
    else {
      this._Search.getSearchIdMovie(id).subscribe((res: any) => {
        this.flag = false;
        console.log(this.movie = res)
      })

    }
  }




}
