import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { MovieSeries, MoviSerieUser } from 'src/app/interfaces/movie-series';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formLogin: FormGroup;
  usuario: User
  datos: any
  faKey = faKey;
  datosC: boolean = false
  user: MoviSerieUser
  constructor(private Form: FormBuilder, private login: AuthService, private router: Router, private db: MoviesService) { }

  ngOnInit(): void {
    this.formLogin = this.Form.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    })
  }


  ingresarGoogle() {
    const { email, password } = this.formLogin.value
    this.login.loginWithGoogle(email, password).then(res => {
      console.log(res),
        this.setLocalStorage(res?.user)
      if (res) {
        this.router.navigate(['/Dashboard']);
      }
    })
  }

  setLocalStorage(data) {
    if (data) {
      localStorage.setItem('Usuario', JSON.stringify(data))
    }
  }
  getLocalStorage() {
    this.datos = JSON.parse(localStorage.getItem('Usuario')||'{}')
    console.log(this.datos.uid)
    this.db.adUser(this.datos.uid)
  }

  Registro() {
    const { email, password } = this.formLogin.value
    this.login.register(email, password).then(res => {
      if (res) {
        console.log(res),
        this.setLocalStorage(res?.user)
        this.getLocalStorage()
        this.router.navigate(['/Dashboard']);
      }

    })
    
  }

}
