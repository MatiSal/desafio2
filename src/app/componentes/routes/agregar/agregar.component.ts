import { Component, OnInit } from '@angular/core';
import { MovieSeries } from 'src/app/interfaces/movie-series';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  movie_series: Array<MovieSeries> = new Array<MovieSeries>();
  movie: Array<MovieSeries> = new Array<MovieSeries>();
  serie: Array<MovieSeries> = new Array<MovieSeries>();
  selectedItem: string;
  datos: any;
  flag: boolean = true
  constructor(private _movieService: MoviesService) { }

  ngOnInit(): void {
    this.getListaMovie()
    this.getListaTv()
  }

  Busca(e: string) {
    console.log(e)
    if (e == '' || e.length <= 3) {
      return this.ngOnInit()
    }
    else {
      this._movieService.getSearch(e).subscribe((res: any) => {
        return console.log(this.movie_series = res.results)
      })

    }
  }

  getLocalStorage(): string {
    this.datos = JSON.parse(localStorage.getItem('Usuario') || '{}')
    return this.datos.uid
  }


  getListaMovie() {
    this._movieService.getLMovieSeries(this.getLocalStorage(), 'movie').subscribe(data => {
      this.movie = [];
      data.forEach((element: any) => {
        this.movie.push({
          idGlobal: element.payload.doc.id,
          ...element.payload.doc.data()
        })
      });
      console.log(this.movie);
    })
  }

  getListaTv() {
    this._movieService.getLMovieSeries(this.getLocalStorage(), 'tv').subscribe(data => {
      this.serie = [];
      data.forEach((element: any) => {
        this.serie.push({
          idGlobal: element.payload.doc.id,
          ...element.payload.doc.data()
        })
      });
      console.log(this.serie);
    })
  }

  compare(movs: MovieSeries): any {

    if (movs.media_type === 'tv') {
      for (let tv of this.serie) {
        if (movs.id == tv.id) {
          return true
        }
        else return false
      }
    }
    else if (movs.media_type === 'movie') {
      for (let mov of this.movie) {
        if (movs.id == mov.id) {
          return true
        }

      }
      return false
    }
  }

  getLocalstorage() {
    this.datos = JSON.parse(localStorage.getItem('Usuario') || '{}')
    console.log(this.datos.uid)
    return this.datos.uid
  }

  agregar(movieSerie: MovieSeries) {
    this._movieService.addMovieSerie(movieSerie, this.getLocalstorage())
      .then(() => {
        console.log('se guardo')
      })
      .catch((error) => {
        console.error('fallo: ', error)
      })
    this.flag = false
  }
  Eliminar(movs: MovieSeries) {
    if (movs.media_type === 'tv') {
      for (let tv of this.serie) {
        if (movs.id == tv.id) {
          this._movieService.deleteMovieSerie(this.getLocalstorage(), tv.idGlobal, movs.media_type)
        }
      }
    }
    else if (movs.media_type === 'movie') {
      for (let mov of this.movie) {
        if (movs.id == mov.id) {
          this._movieService.deleteMovieSerie(this.getLocalstorage(), mov.idGlobal, movs.media_type)
        }
      }
    }

  }
}
