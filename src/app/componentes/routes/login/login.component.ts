import { Component, OnInit, Output } from '@angular/core';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/interfaces/user';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  usuario: User
  faKey = faKey;
  datosC: boolean = false
  datos: any
  constructor(private Form: FormBuilder, private login: AuthService, private router: Router, private db: MoviesService) { }

  ngOnInit(): void {
    this.formLogin = this.Form.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    })
    
  }

  ingresar() {
    const { email, password } = this.formLogin.value
    this.login.login(email, password).then(res => {
      console.log(res),
        this.setLocalStorage(res?.user)
      if (res) {
        this.router.navigate(['/Dashboard']);
        
      }
      this.datosC = true
    })

  }

  ingresarGoogle() {
    const { email, password } = this.formLogin.value
    this.login.loginWithGoogle(email, password).then(res => {
      console.log(res),
        this.setLocalStorage(res?.user)
      if (res) {
        //this.getLocalStorage()
        this.router.navigate(['/Dashboard']);
      }
    })
  }

  setLocalStorage(data) {
    if (data) {
      localStorage.setItem('Usuario', JSON.stringify(data))
    }
  }
  getLocalStorage() {
    this.datos = JSON.parse(localStorage.getItem('Usuario')||'{}')
    console.log(this.datos.uid)
    this.db.adUser(this.datos.uid)
  }

}
