import { style } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { MovieSeries } from 'src/app/interfaces/movie-series';
import { MoviesService } from 'src/app/services/movies.service';
import { CardComponent } from '../../shared/card/card.component';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'], 
  providers: [MoviesService]
})
export class HomeComponent implements OnInit {
  tipo: string = "todo";
  flag: boolean = true;
  selectedItem: string = "";
  movie_series: MovieSeries[]= [];
  movie_series2: Array<MovieSeries> = new Array<MovieSeries>();

  constructor(private _movieService: MoviesService) {

  }

  ngOnInit() {

    this.getTrending();
    
  }

  getTrending(){
    this._movieService.getTrending().subscribe({
      next: (data: any) =>{
        
        this.movie_series = data.results.slice()
        console.log(this.movie_series)
      },
      error: (err) =>{
        console.log(err)
      },
      complete: ()=>{
        console.log('termino')
      }
    })
   
  }

  contador() {
    var cont: number = 0;
    if (this.tipo == 'todo') {
      return this.movie_series.length
    }
    else {
      for (let i = 0; i < this.movie_series.length; i++) {
        if (this.movie_series[i].media_type == this.tipo) {
          cont += 1;
        }
      }
      return cont;
    }
  }



  Busca(e: string) {
    this.movie_series2 = this.movie_series.slice()
    console.log(e)
    if (e == ''  || e.length <= 3) {
      return this.ngOnInit()
    }
    else {
       this._movieService.getSearch(e).subscribe((res: any) =>{
          return console.log(this.movie_series = res.results)
       })
      
    }
  }

  Filter(e) {
    if (e.name == 'todo') {
      this.tipo = e.name;

    }
    else if (e.name == 'movie') {
      this.tipo = e.name;
    }
    else this.tipo = e.name;

  }

}

