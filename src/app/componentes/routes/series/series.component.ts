import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MovieSeries } from 'src/app/interfaces/movie-series';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {
  tipo: string = 'tv';
  movie_series: Array<MovieSeries> = new Array<MovieSeries>();
  movie_series2: Array<MovieSeries> = new Array<MovieSeries>();
  movie_series3: Array<MovieSeries> = new Array<MovieSeries>();
  selectedItem: string;
  public user$: Observable<any> = this.authService.afauth.user
  flag: boolean = false;
  user: any
  constructor(private _seriesSeries: MoviesService,private authService: AuthService, private MovieSerieService: MoviesService) {
  }

  ngOnInit(): void {
    this.getSeries();
    this.getLista()

  }

  getSeries(){
    this._seriesSeries.getSeries().subscribe({
      next: (data: any) =>{
        console.log(data);
        console.log(this.movie_series = data.results )
      },
      error: (err) =>{
        console.log(err)
      },
      complete: ()=>{
        console.log('termino')
      }
    })
   
  }

  getLocalStorage(){
    this.user = JSON.parse(localStorage.getItem('Usuario')||'{}')
    return this.user.uid
  }

  getLista(){
   
    this.MovieSerieService.getLMovieSeries(this.getLocalStorage(),this.tipo).subscribe(data =>{
     this. movie_series2 = [] ;
     data.forEach((element: any) => {
        this. movie_series2.push({
          idGlobal: element.payload.doc.id,
          ...element.payload.doc.data()
        })
      });
      console.log(this. movie_series2);
    })
   }

  Busca(e: string) {
    console.log(e)
    if (e == '' || e.length <= 3) {
      return this.ngOnInit()
    }
    else {
      this._seriesSeries.getSearchSerie(e).subscribe((res: any) =>{
        return console.log(this.movie_series = res.results)
     })
    }
  }

  contador() {
    var cont: number = 0;
    if (this.tipo == 'Serie') {
      for (let i = 0; i < this.movie_series.length; i++) {
        {
          cont += 1;
        }
      }

    }
    return cont = this.movie_series.length;
  }
  BuscaV(e: string){
    if(e == ''){
      this.movie_series2 = this.movie_series3.slice()
    }
  }
  Busca2(e: string) {
    this.movie_series3 = this.movie_series2.slice()
    console.log(e)
    if (e == '' ) {
      return this.getLista()
    }
    else {
    //  this._movie.col(this.getLocalStorage(),'movie', ref=> ref.where('original_title', '==', e.toLowerCase)).subscribe(res =>{
    //    console.log(res)
    //  })
     this.movie_series2 = this.movie_series2.filter( res => {
      return res.name?.toLowerCase().match(e.toLowerCase()) 
      })
      console.log(this.movie_series2)
    }
    
  }
}
