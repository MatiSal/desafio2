import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router, RouterLink } from '@angular/router';
import { Observable } from 'rxjs';
import { MovieSeries, MoviSerieUser } from 'src/app/interfaces/movie-series';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() movie_series: Array<MovieSeries> = new Array<MovieSeries>();
  @Input() tipo: string;
  @Input() flag: boolean;
  user: any
  public user$: Observable<any> = this.authService.afauth.user
  constructor(private router: Router, private authService: AuthService, private MovieSerieService: MoviesService,private firestore: AngularFirestore ) {

  }

  ngOnInit(): void {
    //this.getLista()
    this.user = JSON.parse(localStorage.getItem('Usuario') || '{}')
    
  }

  Detalles(media,id){
    console.log(media)
    this.router.navigate(['/Detalles',media, id  ]);
  }

  contador() {
    var cont: number = 0;
    if (this.tipo == 'Serie') {
      for (let i = 0; i < this.movie_series.length; i++) {
        {
          cont += 1;
        }
      }

    }
    return cont = this.movie_series.length;
  }

  getLocalStorage():string{
    this.user = JSON.parse(localStorage.getItem('Usuario')||'{}')
    return this.user.uid
  }

  getLista(){
   this.MovieSerieService.getLMovieSeries(this.getLocalStorage(),this.tipo).subscribe(data =>{
    this.movie_series = [] ;
    data.forEach((element: any) => {
       this.movie_series.push({
         idGlobal: element.payload.doc.id,
         ...element.payload.doc.data()
       })
     });
     console.log(this.movie_series);
   })
  }

  Eliminar(movs :MovieSeries){
    if(this.tipo == 'movie'){
      this.MovieSerieService.deleteMovieSerie(this.getLocalStorage(),movs.idGlobal,'movie')
    .then(()=>
    console.log("documento borrado", movs.idGlobal))
    .catch((error) => console.error("elemento no borrado",error))
    }
    else{
      this.MovieSerieService.deleteMovieSerie(this.getLocalStorage(),movs.idGlobal,'tv')
    .then(()=>
    console.log("documento borrado", movs.idGlobal))
    .catch((error) => console.error("elemento no borrado",error))
    }
    
  }
}
