import { Genero } from "./genero";

export interface Tv {
    backdrop_path:     null | string;
    first_air_date:    Date;
    genre_ids:         number[];
    id:                number;
    name:              string;
    origin_country:    string[];
    original_language: string;
    original_name:     string;
    overview:          string;
    popularity:        number;
    poster_path:       string;
    vote_average:      number;
    vote_count:        number;
    number_of_seasons: number;
    number_of_episodes: number;
    status: string;
    last_air_date: Date;
    episode_run_time: number[];
    tagline: string;
    genres: Array<Genero>;
}
