import { StringLike } from "@firebase/util";

export interface MovieSeries  extends MoviSerieBase{
    backdrop_path:     string;
    original_name:    string;
    original_title:   string;
    overview:          string;
    release_date:     Date;
    media_type:        string;
    origin_country:   string[];
    idGlobal: string
}

export interface MoviSerieBase{
    id:                number;
    name?:             string;
    poster_path:       string;
    title?:            string;
    vote_average:      number;
}

export interface MoviSerieUser extends MoviSerieBase{
    idUser: String;
}