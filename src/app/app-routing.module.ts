import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './componentes/routes/agregar/agregar.component';
import { DashboardComponent } from './componentes/routes/dashboard/dashboard.component';
import { DetallesComponent } from './componentes/routes/detalles/detalles.component';
import { HomeComponent } from './componentes/routes/home/home.component';
import { LoginComponent } from './componentes/routes/login/login.component';
import { PelisComponent } from './componentes/routes/pelis/pelis.component';
import { RegisterComponent } from './componentes/routes/register/register.component';
import { SeriesComponent } from './componentes/routes/series/series.component';


const routes: Routes = [
  {
    path: 'Inicio', component: HomeComponent
  },
  {
    path: 'Ingresar', component: LoginComponent
  },
  {
    path: 'Pelis', component: PelisComponent
  },
  {
    path: 'Series', component: SeriesComponent
  },
  {
    path: 'Detalles/:media/:id', component: DetallesComponent
  },
  {
    path: 'Registro', component: RegisterComponent
  },
  {
    path: 'Dashboard', component: DashboardComponent
  },
  {
    path: 'Agregar', component: AgregarComponent
  },
  {
    path: '**',
    redirectTo: 'Inicio'
  },
  {
    path: ' ',
    redirectTo: 'Inicio'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
